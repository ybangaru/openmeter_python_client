.. Openmeter Client Docs documentation master file, created by
   sphinx-quickstart on Fri Dec 22 07:13:32 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Openmeter Client Docs's documentation!
=================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
