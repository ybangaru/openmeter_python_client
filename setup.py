from setuptools import setup, find_packages
from os import path

# read the contents of the README file
this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='openmeter_client',
    version='1.0.3',
    description='A Python client to interact with the OpenMeter API endpoints',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=find_packages(where='openmeter'),
    package_dir={'': 'openmeter'},
    install_requires=[
        "requests",
        "pandas",
        "loguru",
    ],
    package_data={
        '': ['docs/*'],
    },
)