# OpenMeter Client

A Python client to interact with the OpenMeter API endpoints.

## Installation

You can install the OpenMeter Client with pip using:

```sh
pip install openmeter_client
```

## Initialization and usage

```python
from openmeter import OpenMeterClient
```

```python
client = OpenMeterClient('your_access_token')
```

For more details and examples please check out https://openmeter-python-client.readthedocs.io/en/latest/openmeter.html#openmeter.client.OpenMeterClient
